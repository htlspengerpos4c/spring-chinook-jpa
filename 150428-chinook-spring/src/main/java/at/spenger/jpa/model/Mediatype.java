package at.spenger.jpa.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the mediatype database table.
 * 
 */
@Entity
@NamedQuery(name="Mediatype.findAll", query="SELECT m FROM Mediatype m")
public class Mediatype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int mediaTypeId;

	private String name;

	//bi-directional many-to-one association to Track
	@OneToMany(mappedBy="mediatype")
	private List<Track> tracks;

	public Mediatype() {
	}

	public int getMediaTypeId() {
		return this.mediaTypeId;
	}

	public void setMediaTypeId(int mediaTypeId) {
		this.mediaTypeId = mediaTypeId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Track> getTracks() {
		return this.tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	public Track addTrack(Track track) {
		getTracks().add(track);
		track.setMediatype(this);

		return track;
	}

	public Track removeTrack(Track track) {
		getTracks().remove(track);
		track.setMediatype(null);

		return track;
	}

}