package at.spenger.jpa.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the invoice database table.
 * 
 */
@Entity
@NamedQuery(name="Invoice.findAll", query="SELECT i FROM Invoice i")
public class Invoice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int invoiceId;

	private String billingAddress;

	private String billingCity;

	private String billingCountry;

	private String billingPostalCode;

	private String billingState;

	@Temporal(TemporalType.TIMESTAMP)
	private Date invoiceDate;

	private BigDecimal total;

	//bi-directional many-to-one association to Customer
	@ManyToOne
	@JoinColumn(name="CustomerId")
	private Customer customer;

	//bi-directional many-to-one association to Invoiceline
	@OneToMany(mappedBy="invoice")
	private List<Invoiceline> invoicelines;

	public Invoice() {
	}

	public int getInvoiceId() {
		return this.invoiceId;
	}

	public void setInvoiceId(int invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getBillingAddress() {
		return this.billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getBillingCity() {
		return this.billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingCountry() {
		return this.billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getBillingPostalCode() {
		return this.billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}

	public String getBillingState() {
		return this.billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<Invoiceline> getInvoicelines() {
		return this.invoicelines;
	}

	public void setInvoicelines(List<Invoiceline> invoicelines) {
		this.invoicelines = invoicelines;
	}

	public Invoiceline addInvoiceline(Invoiceline invoiceline) {
		getInvoicelines().add(invoiceline);
		invoiceline.setInvoice(this);

		return invoiceline;
	}

	public Invoiceline removeInvoiceline(Invoiceline invoiceline) {
		getInvoicelines().remove(invoiceline);
		invoiceline.setInvoice(null);

		return invoiceline;
	}

}