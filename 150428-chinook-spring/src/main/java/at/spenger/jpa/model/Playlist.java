package at.spenger.jpa.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the playlist database table.
 * 
 */
@Entity
@NamedQuery(name="Playlist.findAll", query="SELECT p FROM Playlist p")
public class Playlist implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int playlistId;

	private String name;

	//bi-directional many-to-many association to Track
	//@ManyToMany(fetch=FetchType.EAGER)
	@ManyToMany
	@JoinTable(
		name="playlisttrack"
		, joinColumns={
			@JoinColumn(name="PlaylistId")
			}
		, inverseJoinColumns={
			@JoinColumn(name="TrackId")
			}
		)
	private List<Track> tracks;

	public Playlist() {
	}

	public int getPlaylistId() {
		return this.playlistId;
	}

	public void setPlaylistId(int playlistId) {
		this.playlistId = playlistId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Track> getTracks() {
		return this.tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	@Override
	public String toString() {
		return "Playlist [playlistId=" + playlistId + ", name=" + name
				+ ", tracks=" + getTracks() + "]";
	}
}