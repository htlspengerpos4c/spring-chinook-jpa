package at.spenger.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import at.spenger.jpa.model.Playlist;
import at.spenger.jpa.model.Track;
import at.spenger.jpa.service.PlayListRepository;
import at.spenger.jpa.service.PlayListService;
import at.spenger.jpa.service.TrackRepository;


@Import(Config.class)
public class Application implements CommandLineRunner {

	@Autowired
	private PlayListService playListService;	
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

	@Override
	@Transactional
	public void run(String... arg0) throws Exception {
		testRead();
	}
	
	
	
	private void testRead() {
		List<Playlist> l = playListService.findAll();
		System.out.println(l.get(0));
	}
}
