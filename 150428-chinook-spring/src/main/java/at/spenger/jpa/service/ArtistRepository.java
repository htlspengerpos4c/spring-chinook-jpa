package at.spenger.jpa.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import at.spenger.jpa.model.Artist;

public interface ArtistRepository extends CrudRepository<Artist, Integer> {
	@Query("select a from Artist a where a.name like :name")
	List<Artist> findByNameLike(@Param("name") String name);
}
