package at.spenger.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import at.spenger.jpa.model.Track;
@Component("trackService")
@Transactional
public class TrackServiceImpl implements TrackService {
	private TrackRepository trackRepository;
	
	@Autowired
	public TrackServiceImpl(TrackRepository playListRepository) {
		this.trackRepository = playListRepository;
	}
	
	@Override
	public List<Track> findAll() {
		return trackRepository.findAll();
	}
	
	

}
