package at.spenger.jpa.service;

import java.util.List;

import at.spenger.jpa.model.Artist;

public interface ArtistService  {
	List<Artist> findByNameLike(String name);
	Artist findOne(int id);
	Artist save(Artist a);
	void delete(int id);
	long length();
}
