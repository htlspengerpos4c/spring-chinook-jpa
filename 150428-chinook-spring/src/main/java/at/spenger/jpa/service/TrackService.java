package at.spenger.jpa.service;

import java.util.List;

import at.spenger.jpa.model.Track;

public interface TrackService {
	List<Track> findAll();
}
