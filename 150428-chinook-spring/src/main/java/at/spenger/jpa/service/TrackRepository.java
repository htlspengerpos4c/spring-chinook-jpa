package at.spenger.jpa.service;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import at.spenger.jpa.model.Track;

public interface TrackRepository extends CrudRepository<Track, Integer>{
	List<Track> findAll();
}
